local storage = minetest.get_mod_storage()
local prefix = "newsOnJoinExceptions_"
local pages = 1
local T = minetest.get_translator("news_markdown")

-- Base filenames without language codes. The index in the list corresponds to the page number.
local showed_news = {"tunnelPro", "newBLWeapons", "videomaker"}


local colors = {
	background_color = "#FFF0",
	font_color = "#fff",
	heading_1_color = "#fff",
	heading_2_color = "#fff",
	heading_3_color = "#fff",
	heading_4_color = "#fff",
	heading_5_color = "#fff",
	heading_6_color = "#fff",
	heading_1_size = "26",
	heading_2_size = "24",
	heading_3_size = "22",
	heading_4_size = "20",
	heading_5_size = "18",
	heading_6_size = "16",
	code_block_mono_color = "#8aebf1",
	code_block_font_size = 14,
	mono_color = "#8aebf1",
	block_quote_color = "#FFA",
}

local file_cache = {}

local function get_player_language(name)
	if name then
		local player_info = minetest.get_player_information(name)
		return player_info.lang_code or "en"
	else
		return "en"
	end
end

-- Try to find a corresponding file with lang_code, fallback to English if not found
local function find_news_file(lang_code, page_index)
	local base_filename = showed_news[page_index]
	if not base_filename then return nil end

	local folder_path = minetest.get_modpath("news_markdown") .. "/news/"
	local lang_file = folder_path .. base_filename .. "_" .. lang_code .. ".md"
	local f = io.open(lang_file, "r")
	if f then
		f:close()
		return lang_file
	end

	-- Fallback to English version if localized not found
	local en_file = folder_path .. base_filename .. "_en.md"
	f = io.open(en_file, "r")
	if f then
		f:close()
		return en_file
	end

	return nil
end

local function get_page(name, page)
	local language_code = get_player_language(name)
	local news_filename = find_news_file(language_code, page)
	if not news_filename then return false end

	if file_cache[news_filename] then
		return file_cache[news_filename]
	end

	local news_file = io.open(news_filename, "r")
	if not news_file then
		-- fallback to English explicitly if language file not found
		news_filename = find_news_file("en", page)
		if not news_filename then return false end
		news_file = io.open(news_filename, "r")
		if not news_file then return false end
	end

	local news_markdown = news_file:read("*a")
	news_file:close()

	file_cache[news_filename] = news_markdown
	return news_markdown
end

local function count_news_pages()
	-- Count how many pages we can load by checking each entry in showed_news
	local count = 0
	for i, base_name in ipairs(showed_news) do
		-- Just check if English version exists for counting:
		local folder_path = minetest.get_modpath("news_markdown") .. "/news/"
		local en_file = folder_path .. base_name .. "_en.md"
		local f = io.open(en_file, "r")
		if f then
			f:close()
			count = count + 1
		else
			break
		end
	end
	pages = count
end

-- Update hash based on the English version of the first page
local function update_hash(name)
	local folder_path = minetest.get_modpath("news_markdown") .. "/news/"
	if not showed_news[1] then return end
	local english_news_file = showed_news[1] .. "_en.md"
	local english_news = io.open(folder_path .. english_news_file, "r")

	if not english_news then return end

	storage:set_string(prefix..name, minetest.sha1(english_news:read("*a")))
	english_news:close()

	minetest.log("action", "Updated news hash in mod storage")
end

local function check_for_updates(name)
	local folder_path = minetest.get_modpath("news_markdown") .. "/news/"
	if not showed_news[1] then return end
	local english_news_file = showed_news[1] .. "_en.md"
	local english_news = io.open(folder_path .. english_news_file, "r")
	if not english_news then
		minetest.log("warning", "No " .. english_news_file .. " found in mod folder.")
		return
	end
	local english_news_data = english_news:read("*a")

	if (storage:get_string(prefix..name) ~= minetest.sha1(english_news_data)) then
		storage:set_string(prefix..name, "")
	end

	english_news:close()
end

local function show_news_formspec(name, page)
	page = page or 1
	local page_text = get_page(name, page)
	if not page_text then return end

	-- Use the base filename for the image instead of page number.
	local image_name = showed_news[page] .. ".png"

	local news_formspec =
		"formspec_version[5]" ..
		"size[13,11.3]" ..
		"noprepend[]" ..
		"style_type[image_button;border=false]" ..
		"image[0,0;13,7.5;" .. minetest.formspec_escape(image_name) .. ";]" ..
		"bgcolor[" .. colors.background_color .. "]" ..
		"image_button_exit[5.4,10.05;2,1;news_gui_ok.png;exit;OK]" ..
		"checkbox[0.2,10.7;dont_show_again;" .. minetest.formspec_escape(T("Don't show me this again")) .. ";false]" ..
		"style_type[button_exit;bgcolor=#39314b]"

	if pages > 1 then
		news_formspec =
			news_formspec ..
			"image_button[11,10.3;0.35,0.6;news_gui_arrow_left.png;GO_LEFT;]" ..
			"image_button[12.2,10.3;0.35,0.6;news_gui_arrow_right.png;GO_RIGHT;]" ..
			"label[11.58,10.60;" .. page .. "/" .. pages .. "]"
	end

	news_formspec = news_formspec .. md2f.md2f(0.2, 7.92, 12.7, 2.3, page_text, "server_news", colors)

	minetest.show_formspec(name, "server_news:" .. page, news_formspec)
end

local function open_formspec_on_join(player)
	local name = player:get_player_name()

	if not minetest.get_player_privs(name)["tos_accepted"] then
		return
	end

	check_for_updates(name)

	-- Only show news to players who want to see it
	if storage:get_string(prefix..name) == "" then
		show_news_formspec(name, 1)
	end
end

minetest.register_on_joinplayer(function(player)
	open_formspec_on_join(player)
end)

minetest.register_chatcommand("news", {
    description = "Shows the server news",
	params = "",
    func = function(name) show_news_formspec(name, nil) end
})

minetest.register_on_mods_loaded(function()
	count_news_pages()
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname and not string.find(formname, "server_news:") then return end

	local page = tonumber((formname:split(":")[2]))
	local name = player:get_player_name()

	-- Don't do anything when the exit button is clicked, because no checkbox data is sent then
	if not fields.exit then
		if fields.dont_show_again == "true" then
			update_hash(name)
		else
			storage:set_string(prefix..name, "")
		end

		if fields.GO_LEFT then
			page = math.max(1, page-1)
			show_news_formspec(name, page)

		elseif fields.GO_RIGHT then
			page = math.min(pages, page+1)
			show_news_formspec(name, page)
		end
	end
end)
