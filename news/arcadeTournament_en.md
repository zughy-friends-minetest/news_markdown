# The first arcade tournament is here!

Introducing you the new free-for-all AES tournament on the **22th of December, 19:30-21:30 UTC+0**. Come have fun and play through all the different arcade minigames. Apply on our matrix space ```#minetest-aes:matrix.org``` using a client like Element.