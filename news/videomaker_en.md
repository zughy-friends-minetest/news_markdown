# "Videomaker" chat prefix!

Do you make **videos** on Youtube/Peertube? Let everyone know, and redeem your "Videomaker" chat prefix!
For more info join our matrix room ```#minetest-aes:matrix.org```.