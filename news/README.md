File names should be formatted `<yourNewsTopic>_<countryCode>.md`. The image should be named `<yourNewsTopic>.png` and be put under the textures folder (resolution 1280x720).

Then to edit the displayed news, modify `showed_news` in `init.lua`.